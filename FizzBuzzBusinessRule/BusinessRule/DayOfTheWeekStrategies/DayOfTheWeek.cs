﻿namespace FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies
{
    using System;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies.Definitions;

    /// <summary>
    /// DayOfTheWeek Class.
    /// </summary>
    public class DayOfTheWeek : IDayOfTheWeek
    {
        /// <summary>
        /// Is Day Of Week .
        /// </summary>
        /// <param name="dayOfWeek">The  Day Of Week.</param>
        /// <returns>Whether it is  Day Of Week or not.</returns>
        public bool IsDayOfWeek(string dayOfWeek)
        {
            return DateTime.Today.DayOfWeek.ToString() == dayOfWeek;
        }
    }
}
