﻿namespace FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// DayOfTheWeekStrategy Interface.
    /// </summary>
    public interface IDayOfTheWeekStrategy
    {
        /// <summary>
        /// Day of the week
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>String.</returns>
        string DayOfTheWeekStrategy(string input);
    }
}
