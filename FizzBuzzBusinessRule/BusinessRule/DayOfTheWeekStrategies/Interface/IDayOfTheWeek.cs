﻿namespace FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies
{
    /// <summary>
    /// DayOfTheWeek Interface.
    /// </summary>
    public interface IDayOfTheWeek
    {
        /// <summary>
        /// Day of the week
        /// </summary>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>Whether it is  Day Of Week or not..</returns>
        bool IsDayOfWeek(string dayOfWeek);
    }
}
