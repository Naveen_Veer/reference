﻿namespace FizzBuzzBusinessRule.BusinessRules.DayOfWeekStrategies.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using BusinessRule.DayOfTheWeekStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies;

    /// <summary>
    /// WednesDayRules class.
    /// </summary>
    public class WednesdayRule : IDayOfTheWeekStrategy
    {
        /// <summary>
        /// Day of the week.
        /// </summary>
        private IDayOfTheWeek dayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="WednesdayRule"/> class.
        /// </summary>
        /// <param name="dayOfWeek">The day of week.</param>
        public WednesdayRule(IDayOfTheWeek dayOfWeek)
        {
            this.dayOfWeek = dayOfWeek;
        }

        /// <summary>
        /// verifies if wednesday and returns the expected result.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>result.</returns>
        public string DayOfTheWeekStrategy(string input)
        {
            var result = string.Empty;

            if (this.dayOfWeek.IsDayOfWeek(Constants.Wednesday))
            {
                if (input == Constants.FizzBuzz)
                {
                    result = Constants.WizzWuzz;
                }
                else if (input == Constants.Buzz)
                {
                    result = Constants.Wuzz;
                }
                else if (input == Constants.Fizz)
                {
                    result = Constants.Wizz;
                }               
            }
            else
            {
                result = input;
            }
           
            return result;
        }
    }
}