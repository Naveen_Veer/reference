﻿namespace FizzBuzzBusinessRule
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web;
    using BusinessRule.DayOfTheWeekStrategies.Definitions;
    using BusinessRules.FizzBuzzStrategies.Definitions;
    using BusinessRules.Interface;

    /// <summary>
    /// The FizzBuzzBusinessFacade class.
    /// </summary>
    public class FizzBuzzBusinessRules : IFizzBuzzBusiness
    {
        /// <summary>
        /// List Of Divisiblity strategies.
        /// </summary>
        private readonly IList<IDivisibiltyStrategy> strategies;

        /// <summary>
        /// List Of day of week strategies.
        /// </summary>
        private readonly IList<IDayOfTheWeekStrategy> dayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzBusinessRules"/> class.
        /// </summary>
        /// <param name="strategies">List Of Divisible strategies.</param>
        /// <param name="dayOfWeek">List Of day of week strategies.</param>
        public FizzBuzzBusinessRules(
            IList<IDivisibiltyStrategy> strategies,
            IList<IDayOfTheWeekStrategy> dayOfWeek)
        {
            this.strategies = strategies;
            this.dayOfWeek = dayOfWeek;
        }

        /// <summary>
        /// Gets the fizz buzz display List.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Return Fizz Buzz Series.</returns>
        public IEnumerable<string> GetFizzBuzzDisplayList(int number)
        {
            var resultList = new List<string>();
            for (int input = 1; input <= number; input++)
            {
                var matchedStrategy = this.strategies.FirstOrDefault(x => x.IsNumberDivisible(input) != string.Empty);
                var matchedOutput = string.Empty;
                var finalOutput = string.Empty;

                if (matchedStrategy != null)
                {
                    matchedOutput = matchedStrategy.IsNumberDivisible(input);
                    if (!string.IsNullOrEmpty(matchedOutput))
                     {
                         finalOutput = this.VerifyTheDayOfTheWeek(matchedOutput);
                     }
                }

                resultList.Add(finalOutput.Length > 0 ? finalOutput : input.ToString());
            }

            return resultList;
        }

        /// <summary>
        /// VerifyTheDay Method.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The matched result.</returns>
        private string VerifyTheDayOfTheWeek(string input)
        {
            var matchingStrategy = this.dayOfWeek.FirstOrDefault(x => x.DayOfTheWeekStrategy(input) != string.Empty);
            var matchedResult = string.Empty;

            if (matchingStrategy != null)
            {
                matchedResult = matchingStrategy.DayOfTheWeekStrategy(input);
            }

            return matchedResult;
        }
    }
}