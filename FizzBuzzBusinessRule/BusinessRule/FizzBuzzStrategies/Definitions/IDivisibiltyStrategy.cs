﻿namespace FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Definitions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Divisibility strategies interface
    /// </summary>
    public interface IDivisibiltyStrategy
    {
        /// <summary>
        /// Is Divisible Method.
        /// </summary>
        /// <param name="number">The number parameter.</param>
        /// <returns>Whether it is divisible or not.</returns>
        string IsNumberDivisible(int number);
    }
}
