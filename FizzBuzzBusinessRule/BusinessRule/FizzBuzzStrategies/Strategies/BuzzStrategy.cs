﻿namespace FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using BusinessRules.FizzBuzzStrategies.Definitions;
   
    /// <summary>
    /// The Buzz class.
    /// </summary>
    public class BuzzStrategy : IDivisibiltyStrategy
    {
        /// <summary>
        /// Is Divisible Method.
        /// </summary>
        /// <param name="number">The inputNumber parameter.</param>
        /// <returns>Whether it is divisible by 5.</returns>
        public string IsNumberDivisible(int number)
        {
            var result = string.Empty;

            if (number % 5 == 0 && number % 3 != 0)
            {
                result = Constants.Buzz;
            }

            return result;
            }
    }
}