﻿namespace FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using BusinessRules.FizzBuzzStrategies.Definitions;

    /// <summary>
    /// FizzBuzz class.
    /// </summary>
    public class FizzBuzzStrategy : IDivisibiltyStrategy
    {
        /// <summary>
        /// Is Number Divisible Method.
        /// </summary>
        /// <param name="inputNumber">The Number parameter.</param>
        /// <returns>Whether it is divisible by 3 and 5.</returns>
        public string IsNumberDivisible(int number)
        {
            var result = string.Empty;

            if (number % 3 == 0 && number % 5 == 0)
            {
                result = Constants.FizzBuzz;
            }

            return result;
         }
    }
}