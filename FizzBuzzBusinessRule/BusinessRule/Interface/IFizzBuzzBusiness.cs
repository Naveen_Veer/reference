﻿namespace FizzBuzzBusinessRule.BusinessRules.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for fizz buss business rule class.
    /// </summary>
   public interface IFizzBuzzBusiness
    {
        /// <summary>
        /// Gets the fizz buzz display list.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>Return Fizz Buzz Series.</returns>
       IEnumerable<string> GetFizzBuzzDisplayList(int number);
    }
}
