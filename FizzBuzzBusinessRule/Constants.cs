﻿// <copyright file = "Constants.cs" company="TCS"> 
// Copyright(C) FizzBuzz 2017 All Rights Reserved 
// </copyright>

namespace FizzBuzzBusinessRule
{
    /// <summary>
    /// Application Constant class.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Business Day  Of Week.
        /// </summary>
        public const string Wednesday = "Wednesday";

        /// <summary>
        /// The FIZZ.
        /// </summary>
        public const string Fizz = "Fizz";

        /// <summary>
        /// The BUZZ.
        /// </summary>
        public const string Buzz = "Buzz";

        /// <summary>
        /// FIZZ BUZZ.
        /// </summary>
        public const string FizzBuzz = "Fizz Buzz";

        /// <summary>
        /// The WIZZ.
        /// </summary>
        public const string Wizz = "Wizz";

        /// <summary>
        /// The WUZZ.
        /// </summary>
        public const string Wuzz = "Wuzz";

        /// <summary>
        /// WIZZ WUZZ.
        /// </summary>
        public const string WizzWuzz = "Wizz Wuzz";
    }
}