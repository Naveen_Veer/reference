﻿namespace FizzBuzzRuleTest.BusinessRuleTest
{
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Strategies;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;

    [TestClass]
    public class BuzzStrategyTest
    {
        /// <summary>
        /// DivisibleByThree object.
        /// </summary>
        private IDivisibiltyStrategy buzzStrategy;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.buzzStrategy = new BuzzStrategy();
        }

        /// <summary>
        /// Test to return buzz. 
        /// </summary>
        [Test]
        public void TestFor_ReturBuzz()
        {
            // Arrange.
            int input = 5;

            // Act.
            var result = this.buzzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual("Buzz", result);
        }

        /// <summary>
        /// Test to return null if not divisible by 5.
        /// </summary>
        [Test]
        public void TestFor_ReturNull()
        {
            // Arrange.
            int input = 2;

            // Act.
            var result = this.buzzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }
    }
}
