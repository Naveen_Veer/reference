﻿namespace FizzBuzzRuleTests.BusinessRuleTest
{
    using System;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies;

    /// <summary>
    /// Summary description for DayOfTheWeekTest
    /// </summary>
    [TestClass]
    public class DayOfTheWeekTest
    {
        /// <summary>
        /// The date time provider object.
        /// </summary>
        private IDayOfTheWeek dayOfTheWeek;

        /// <summary>
        /// Test for IsBusiness Day Of Week Returns True.
        /// </summary>
        [Test]
        public void TestFor_IsBusinessDayOfWeek_ReturnTrue()
        {
            // Arrange.
            this.dayOfTheWeek = new DayOfTheWeek();
            var businessDay = DateTime.Today.DayOfWeek.ToString();

            // Act. 
            var result = this.dayOfTheWeek.IsDayOfWeek(businessDay);

            // Assert.
            NUnit.Framework.Assert.IsTrue(result);
        }

        /// <summary>
        /// Test for IsBusiness Day Of Week Returns False
        /// </summary>
        [Test]
        public void TestFor_IsBusinessDayOfWeek_ReturnFalse()
        {
            // Arrange.
            this.dayOfTheWeek = new DayOfTheWeek();
            var businessDay = DateTime.Now.AddDays(1).DayOfWeek.ToString();

            // Act.
            var result = this.dayOfTheWeek.IsDayOfWeek(businessDay);

            // Assert.
            NUnit.Framework.Assert.IsFalse(result);
        }
    }
}
