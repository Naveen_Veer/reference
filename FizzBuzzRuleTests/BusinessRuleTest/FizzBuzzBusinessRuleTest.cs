﻿namespace FizzBuzzRuleTests.BusinessRuleTest
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzBusinessRule;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Definitions;
    using Moq;
    using NUnit.Framework;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies;

    public class FizzBuzzBusinessRuletest
    {
        /// <summary>
        /// The fizz buzz business rules.
        /// </summary>
        private FizzBuzzBusinessRules fizzBuzzBusiness;

        /// <summary>
        /// Mocking fizz startegies.
        /// </summary>
        private Mock<IDivisibiltyStrategy> mockFizzStrategies;

        /// <summary>
        /// Mocking buzz startegies.
        /// </summary>
        private Mock<IDivisibiltyStrategy> mockBuzzStrategies;

        /// <summary>
        /// Mocking fizz buzz startegies.
        /// </summary>
        private Mock<IDivisibiltyStrategy> mockFizzBuzzStrategies;

        private IList<IDivisibiltyStrategy> listOfDivisibles;

        /// <summary>
        /// Mocking day of the week.
        /// </summary>
        private Mock<IDayOfTheWeekStrategy> mockDayOfTheWeek;

        private Mock<IDayOfTheWeek> mockToday;

        private IList<IDayOfTheWeekStrategy> dayOfTheWeekStrategies;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.listOfDivisibles = new List<IDivisibiltyStrategy>();

            this.dayOfTheWeekStrategies = new List<IDayOfTheWeekStrategy>();

            this.mockFizzStrategies = new Mock<IDivisibiltyStrategy>();

            this.mockBuzzStrategies = new Mock<IDivisibiltyStrategy>();

            this.mockFizzBuzzStrategies = new Mock<IDivisibiltyStrategy>();

            this.mockToday = new Mock<IDayOfTheWeek>();

            this.mockDayOfTheWeek = new Mock<IDayOfTheWeekStrategy>();

            this.dayOfTheWeekStrategies.Add(this.mockDayOfTheWeek.Object);

            this.fizzBuzzBusiness = new FizzBuzzBusinessRules(this.listOfDivisibles, this.dayOfTheWeekStrategies);
        }

        /// <summary>
        /// Test for Get Fizz Buzz Series for InValid Inputs.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithReturnEmptyList()
        {
            // Arrange
            var expectedResult = new List<string>();

            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(0);

            // Assert.
            Assert.AreEqual(expectedResult, resultList);
        }

        /// <summary>
        /// Test Get Fizz Buzz Series When Input Is Fifteen And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsThree_ForNonBusinessDay_ReturnFizz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.Fizz)).Returns(false);
            this.mockFizzStrategies.Setup(a => a.IsNumberDivisible(3)).Returns("Fizz");
            this.listOfDivisibles.Add(this.mockFizzStrategies.Object);
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Fizz");
            
            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(3);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(3));
            resultList.ElementAt(2).Equals(Constants.Fizz);
        }

        /// <summary>
        /// Test Get Fizz Buzz Series When Input Is Fifteen And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsThree_ForBusinessDay_ReturnWizz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.Fizz)).Returns(true);
            this.mockFizzStrategies.Setup(a => a.IsNumberDivisible(3)).Returns("Wizz");
            this.listOfDivisibles.Add(this.mockFizzStrategies.Object);
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Wizz");

            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(3);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(3));
            resultList.ElementAt(2).Equals(Constants.Wizz);
        }

        /// <summary>
        /// Test Get Fizz Buzz Series When Input Is Fifteen And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsThree_ForNonBusinessDay_ReturnBuzz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.Buzz)).Returns(false);
            this.mockBuzzStrategies.Setup(a => a.IsNumberDivisible(5)).Returns("Buzz");

            this.listOfDivisibles.Add(this.mockBuzzStrategies.Object);
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Buzz");

            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(5);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(5));
            resultList.ElementAt(4).Equals(Constants.Buzz);
        }

        /// <summary>
        /// Test Get Wuzz  When Input Is five And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsThree_ForBusinessDay_ReturnWuzz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.Wuzz)).Returns(false);
            this.mockBuzzStrategies.Setup(a => a.IsNumberDivisible(5)).Returns("Wuzz");

            this.listOfDivisibles.Add(this.mockBuzzStrategies.Object);
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Wuzz");

            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(5);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(5));
            resultList.ElementAt(4).Equals(Constants.Wuzz);
        }

        /// <summary>
        /// Test Get Fizz Buzz Series When Input Is Fifteen And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsFifteen_ForNonBusinessDay_ReturnFizzBuzz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.FizzBuzz)).Returns(false);
            this.mockFizzBuzzStrategies.Setup(a => a.IsNumberDivisible(15)).Returns("Fizz Buzz");
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Fizz Buzz");
            this.listOfDivisibles.Add(this.mockFizzBuzzStrategies.Object);
            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(15);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(15));
            resultList.ElementAt(14).Equals(Constants.FizzBuzz);
        }

        /// <summary>
        /// Test Get Wizz Wuzz When Input Is Fifteen And BusinessDayOfWeek is True.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsFifteen_ForBusinessDay_ReturnizzWuzz()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(Constants.WizzWuzz)).Returns(false);
            this.mockFizzBuzzStrategies.Setup(a => a.IsNumberDivisible(15)).Returns("Wizz Wuzz");
            this.mockDayOfTheWeek.Setup(x => x.DayOfTheWeekStrategy(It.IsAny<string>())).Returns("Wizz Wuzz");
            this.listOfDivisibles.Add(this.mockFizzBuzzStrategies.Object);
            // Act.
            var resultList = this.fizzBuzzBusiness.GetFizzBuzzDisplayList(15);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(15));
            resultList.ElementAt(14).Equals(Constants.WizzWuzz);
        }
    }
}