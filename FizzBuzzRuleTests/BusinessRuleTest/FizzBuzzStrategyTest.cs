﻿namespace FizzBuzzRuleTest.BusinessRuleTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Strategies;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;

    [TestClass]
   public class FizzBuzzStrategyTest
    {
        /// <summary>
        /// DivisibleByThree object.
        /// </summary>
        private IDivisibiltyStrategy fizzBuzzStrategy;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.fizzBuzzStrategy = new FizzBuzzStrategy();
        }

        /// <summary>
        /// Test to return FizzBuzz. 
        /// </summary>
        [Test]
        public void TestFor_ReturnFizzBuzz()
        {
            // Arrange.
            int input = 15;

            // Act.
            var result = this.fizzBuzzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual("Fizz Buzz", result);
        }

        /// <summary>
        /// Test to return null if not divisible by 3 and 5.
        /// </summary>
        [Test]
        public void TestFor_ReturnNull()
        {
            // Arrange.
            int input = 1;

            // Act.
            var result = this.fizzBuzzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }
    }
}
