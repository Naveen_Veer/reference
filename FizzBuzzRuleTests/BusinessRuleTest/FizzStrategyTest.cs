﻿namespace FizzBuzzRuleTest.BusinessRuleTest
{
    using System;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRules.FizzBuzzStrategies.Strategies;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;

    [TestClass]
    public class FizzStrategyTest
    {
        /// <summary>
        /// DivisibleByThree object.
        /// </summary>
        private IDivisibiltyStrategy fizzStrategy;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.fizzStrategy = new FizzStrategy();
        }

        /// <summary>
        /// Test to return fizz 
        /// </summary>
        [Test]
        public void TestFor_ReturnFizz()
        {
            // Arrange.
            int input = 3;

            // Act.
            var result = this.fizzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual("Fizz", result);
        }

        /// <summary>
        /// Test to return null if not divisible by 3.
        /// </summary>
        [Test]
        public void TestFor_ReturnNull()
        {
            // Arrange.
            int input = 2;

            // Act.
            var result = this.fizzStrategy.IsNumberDivisible(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }
    }
}
