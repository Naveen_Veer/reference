﻿namespace Tests.BusinessRuleTest
{
    using System;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies.Definitions;
    using FizzBuzzBusinessRule.BusinessRules.DayOfWeekStrategies.Strategies;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using FizzBuzzBusinessRule.BusinessRule.DayOfTheWeekStrategies;

    [TestClass]
    public class WednesdayRuleTest
    {
        /// <summary>
        /// mock day of the week.
        /// </summary>
        private Mock<IDayOfTheWeek> mockToday;

        /// <summary>
        /// wednesdayrule.
        /// </summary>
        private WednesdayRule wednesdayRule;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.mockToday = new Mock<IDayOfTheWeek>();
            this.wednesdayRule = new WednesdayRule(this.mockToday.Object);
        }

        /// <summary>
        /// Test case to return wizz if wednesday. 
        /// </summary>
        [Test]
        public void TestFor_OtherBusinessDay()
        {
            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(It.IsAny<string>())).Returns(false);

            // Act
            string resultString = this.wednesdayRule.DayOfTheWeekStrategy("Fizz");

            // Assert.
            NUnit.Framework.Assert.AreEqual("Fizz", resultString);
        }

        [Test]
        public void TestFor_Wednesday()
        {
            var day = DayOfWeek.Wednesday.ToString();
            string input = "Fizz";

            // Arrange.
            this.mockToday.Setup(a => a.IsDayOfWeek(It.IsAny<string>())).Returns(true);

            // Act
            var resultString = this.wednesdayRule.DayOfTheWeekStrategy(input);

            // Assert.
            NUnit.Framework.Assert.AreEqual("Wizz", resultString);
        }
    }
}
