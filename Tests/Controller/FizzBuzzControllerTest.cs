﻿namespace FizzBuzzRuleTest.ControllerTest
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Controllers;
    using FizzBuzzApplication.Models;
    using FizzBuzzBusinessRule.BusinessRules.Interface;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// FizzBuzzController
        /// </summary>
        private FizzBuzzController fizzBuzzController;

        /// <summary>
        /// Mock fizz buzz business rule.
        /// </summary>
        private Mock<IFizzBuzzBusiness> mockFizzBuzzBusinessRule;

        /// <summary>
        /// FizzBuzz View.
        /// </summary>
        private FizzBuzzModel fizzBuzzModel;

        /// <summary>
        /// Configuration method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.mockFizzBuzzBusinessRule = new Mock<IFizzBuzzBusiness>();
            this.fizzBuzzController = new FizzBuzzController(this.mockFizzBuzzBusinessRule.Object);
        }

        /// <summary>
        /// TestFor Index Method For HomePage.
        /// </summary>
        [Test]
        public void TestFor_FizzBuzz_View()
        {
            // Arrange
            this.fizzBuzzModel = new FizzBuzzModel();

            // Act.
            var result = this.fizzBuzzController.FizzBuzz(this.fizzBuzzModel, 0);

            // Assert
            result.Should().BeOfType<ViewResult>();
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Pagination PageIndex Is Two Return FizzBuzz Series
        /// </summary>
        [Test]
        public void ReturnFizzBuzzSeries_forPagination2()
        {
            // Arrange.
            var list = new List<string>() 
            { 
                "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", 
                "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23",
                "Fizz", "Buzz" 
            };

            this.mockFizzBuzzBusinessRule.Setup(a => a.GetFizzBuzzDisplayList(It.IsAny<int>())).Returns(list);
            var fizzBuzzModel = new FizzBuzzModel { Number = 25 };

            // Act.
            var result = this.fizzBuzzController.FizzBuzzView(fizzBuzzModel, 2);
            var resultModel = (FizzBuzzModel)result.ViewData.Model;

            // Assert.
            resultModel.FizzBuzzList.Should().HaveCount(5);
            this.mockFizzBuzzBusinessRule.Verify(a => a.GetFizzBuzzDisplayList(It.IsAny<int>()), Times.Once());
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Pagination PageIndex Is one Return FizzBuzz Series
        /// </summary>
        [Test]
        public void ReturnFizzBuzzSeriesFor_Pagination1()
        {
            // Arrange.
            var list = new List<string>() 
            { 
                "1", "2", "Fizz", "4", "Buzz", "Fizz",
                "7", "8", "Fizz", "Buzz", "11", "Fizz", "13",
                "14", "Fizz Buzz", "16", "17", "Fizz", "19",
                "Buzz", "Fizz", "22", "23", "Fizz", "Buzz" 
            };

            this.mockFizzBuzzBusinessRule.Setup(a => a.GetFizzBuzzDisplayList(It.IsAny<int>())).Returns(list);
            var fizzBuzzModel = new FizzBuzzModel { Number = 25 };

            // Act.
            var result = this.fizzBuzzController.FizzBuzzView(fizzBuzzModel, 1);
            var resultModel = (FizzBuzzModel)result.ViewData.Model;

            // Assert.
            resultModel.FizzBuzzList.Should().HaveCount(20);
            this.mockFizzBuzzBusinessRule.Verify(a => a.GetFizzBuzzDisplayList(It.IsAny<int>()), Times.Once());
        }

        [Test]
        public void ReturnModelWhenInputIs2()
        {
            // Arrange
            this.fizzBuzzModel = new FizzBuzzModel();

            this.mockFizzBuzzBusinessRule.Setup(t => t.GetFizzBuzzDisplayList(It.IsAny<int>())).Returns(new List<string>()
            {
                " 1", " 2"
            });

            // Act
            ViewResult actionResult = (ViewResult)this.fizzBuzzController.FizzBuzzView(this.fizzBuzzModel, 1);
            var model = actionResult.Model;

            // Assert
            NUnit.Framework.Assert.IsNotNull(model);
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        public void TestFor_FizzBuzzResult_WithValidInput_ReturnFizzBuzzSeries()
        {
            // Arrange.
            this.mockFizzBuzzBusinessRule.Setup(a => a.GetFizzBuzzDisplayList(It.IsAny<int>())).Returns(new List<string> { "1", "2", "Fizz" });
            var fizzBuzzModel = new FizzBuzzModel { Number = 3 };

            // Act.
            var result = this.fizzBuzzController.FizzBuzz(fizzBuzzModel, 1);
            var resultModel = (FizzBuzzModel)result.ViewData.Model;

            // Assert.
            resultModel.FizzBuzzList.Should().HaveCount(3);
            this.mockFizzBuzzBusinessRule.Verify(a => a.GetFizzBuzzDisplayList(It.IsAny<int>()), Times.Once());
        }
    }
}